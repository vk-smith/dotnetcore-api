# DotNet Core WebApi CD PoC #

# TL;DR
* Objective was to setup a [CD](https://en.wikipedia.org/wiki/Continuous_delivery) and then [CI](https://en.wikipedia.org/wiki/Continuous_integration) for dotnet core web api running in docker images on [Aws Ecs](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
* Previously flow has been integrated to deploy docker images from [S3 bucket](https://aws.amazon.com/documentation/s3/)
* This PoC focuses on build flow from Bitbucket repo to S3 bucket
* Two alternatives have been implemented: using Jenkins server and using [Bibucket pipelines](https://bitbucket.org/product/features/pipelines)

# Source Control System #
* Bitbucket Cloud has been used for the benefits of [Bibucket pipelines](https://bitbucket.org/product/features/pipelines)
* Currently Bitbucket Cloud repo has been set to public for the public access purposes
* Bitbucket Server currently does not support pipelines and hence Jenkins integration was used
* Bitbucket Server however supports hooks and can be integrated with Jenkins Build Server. Important 'Poll SCM' must be enabled in project config.

# Common Pre-Requisites #
* To build dotnet core sdk rather than runtime is required
* To push docker image to S3 bucket aws cli is required

# Bitbucket-pipelines #
* Docker support needs to be enabled in the yml: https://blog.bitbucket.org/2017/04/18/bitbucket-pipelines-supports-building-docker-images-and-service-containers/
* Custom build image has been provisioned to support dotnet core sdk + aws cli: https://github.com/vkhazin/dotnetcore-build
* Build scriph: https://bitbucket.org/vk-smith/dotnetapi/src/master/ci-build.sh
* Pipeline yml: https://bitbucket.org/vk-smith/dotnetapi/src/master/bitbucket-pipelines.yml
* Pipelines are triggered on each commit once enabled in settings

# Jenkins Build Server on Ubuntu 16.06
* Due to verbosity documented in a [separate repo](https://github.com/vkhazin/dotnetcore-build-jenkins)